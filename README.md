# Pokebot
[![standard-readme compliant](https://img.shields.io/badge/readme%20style-standard-brightgreen.svg?style=flat-square)](https://github.com/RichardLitt/standard-readme)

## Description
This project is an assignment from [Noroff](https://www.noroff.no/en/), as part of [Experis Academy](https://www.experis.se/sv/it-tjanster/experis-academy). 
<br />

## Table of Contents
- [Background](#background)
- [Implementation](#implementation)
- [Requirements](#requirements)
- [Usage](#usage)
- [API](#api)
- [Contributing](#contributing)
- [License](#license)

## Background
[Pokemon:](https://en.wikipedia.org/wiki/Pok%C3%A9mon) In Pokémon, humans, known as Pokémon Trainers, catch and train Pokémon to battle other Pokémon for sport.

In Pokemon, players play as Pokemon trainers where you catch creatures called Pokemon with a device called a Pokeball. In the games, players collect Pokemon, train and level them up and fight other Pokemon and trainers. The main goal for the player is to collect all the Pokemon and winning the Pokemon league.

REST Architecture: REpresentational State Transfer (REST) is an architecture design specification for distributed media systems. Most modern internal and external network systems that rely on sending media will incorporate using REST somewhere within the system's operation pipeline. REST relies on HTTP request/response messages as a transportation form, but the validation, data fetching, encryption, and other operations, relies of the REST application programming interface (API) to fulfil.

[PokeApi:](https://pokeapi.co/) This is a full RESTful API linked to an extensive database detailing everything about the Pokémon main game series.

We've covered everything from Pokémon to Berry Flavors.

### Assignment
"Your program is tasked to do the following:"

    - Generate a random integer between 1 and 898 inclusive.
    - Connect to the public Pokemon API and request a Pokemon using an ID from the randomly generated number.
    - The program must then query the GPRS and see if the Pokemon has been caught before, if it has then end the program immediately.
    - The Pokemon's information must then be stored on the GPRS as single JSON object string with the following properties:
        - The Pokemon's Name
        - The Pokemon's Type, can have up to two.
        - The Pokemon's Front Default sprite URL.
    - The meta data for this object must be "pokemon

## Implementation
The program makes a call to the PokeApI with a random number. It then checks if that pokemon is in the local database of the user. If it is then the program stops, otherwise the data of the name, types and front sprite of the pokemon gets added to the database.

![alt picture](pokebot.png)
![alt picture](catching.png)
## Requirements
* [Python generic DB server](https://gitlab.com/noroff-accelerate/embedded-bootcamp/projects/python-generic-db-rest-server)

Required :
```
git clone https://gitlab.com/noroff-accelerate/embedded-bootcamp/projects/python-generic-db-rest-server
cd python-generic-db-rest-server
pip3 install -r requirements.txt
python3 src/main.py
```
Install the program:
```
- Clone the repo.
- Install and run python server
- Run cmake
```

## Usage

```
- Run ./build/Pokebot
```
A pokemon from the PokeAPI will be randomly chosen and added to the localhost database. To see the Pokemons caught go to this [link](http://127.0.0.1:5000/fetch/all)(only work if the server is running)

## API
- [Restclient-cpp](https://github.com/mrtazz/restclient-cpp)
- [nlohmann/json](https://github.com/nlohmann/json)
- [PokeAPI](https://pokeapi.co/)

## Contributing
- Martin Dahrén
- Paal Marius Haugen

### Martin Dahrén
- Backend programming
### Paal Marius Bruun Haugen
- Server Communication

## License
[MIT](docs/LICENSE.md)

