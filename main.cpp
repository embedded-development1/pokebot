#include <iostream>
#include <cstdlib>
#include <time.h>
#include "restclient-cpp/restclient.h"
#include <nlohmann/json.hpp>
using json = nlohmann::json;
json createJSONBlock(RestClient::Response response);
int getRandomNum(){
    srand(time(0));
    return (1 + (rand() % 898));
}

RestClient::Response GetPokemon(int Id){
    std::string idText = std::to_string(Id);
    RestClient::Response adress = RestClient::get("https://pokeapi.co/api/v2/pokemon/" + idText);
    return adress;
}

void submitToDatabase(json json_body)
{
    json DataToSend = {{"meta", "pokemon"}, {"data", json_body.dump()}}; // convert data into a json that is ready for python sending

    auto r = RestClient::get("http://127.0.0.1:5000/fetch");

    bool allreadyCaught = false;
    std::string senddata = DataToSend["data"];
    json parsedSendData = json::parse(senddata); //parsing data so you can get name

    for(auto pokemon : json::parse(r.body))
    {
        std::string pokedata = pokemon["data"]; //getting each pokemon and parsing them so you can get the name to check if you allready have them
        json parsedData = json::parse(pokedata);
        if(parsedData["name"] == parsedSendData["name"])
        {
            allreadyCaught = true;
        }
    }
    if(!allreadyCaught)
    {
        auto respo = RestClient::post("http://127.0.0.1:5000/submit", "application/json", DataToSend.dump()); // send data to python server
        std::cout << "You caught: " << parsedSendData["name"] << "!" << std::endl;
    }
    else 
    {
        std::cout << "You allready have: " << parsedSendData["name"] << std::endl;
    }
}

json createJSONBlock(RestClient::Response response){
    json j;
    auto json_body = json::parse(response.body);
    j["name"] = json_body["name"];
    const auto &types = json_body["types"];
    std::string typeArray[2];
    for (size_t i = 0; i < types.size(); i++){
        typeArray[i] = types[i]["type"]["name"];
    }
    j["Types"] = typeArray;
    j["Front Sprite"] = json_body["sprites"]["front_default"];
    return j;
}

int main(){
    int pokeId = getRandomNum();
    RestClient::Response newPokemon = GetPokemon(pokeId);
    //std::cout << "Body: " << newPokemon.body << "\n";

    json json_body = createJSONBlock(newPokemon);
    std::cout << "Catching: " << json_body["name"] << std::endl;

    submitToDatabase(json_body);

    return 0;
}